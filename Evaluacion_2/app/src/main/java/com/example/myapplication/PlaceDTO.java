package com.example.myapplication;

public class PlaceDTO {

    private String name;
    private String description;
    private Integer coordinate;

    public String getName(){
        return name;
    }
    public String setName(){
        return name;
    }
    public String getDescription(){
        return description;
    }
    public String setDescription(){
        return description;
    }
    public Integer getCoordinate() {
        return coordinate;
    }
    public  Integer setCoordinate(){
        return coordinate;
    }
}

