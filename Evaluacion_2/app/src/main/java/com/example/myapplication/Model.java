package com.example.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Model {
    public SQLiteDatabase getCon(Context context){
        SQLiteConnect connect = new SQLiteConnect(context, "dbplace",null, 1 );
        SQLiteDatabase database = connect.getWritableDatabase();
        return database;
    }

    int interPlace(Context context, PlaceDTO placeDTO){
        int response = 0;
        String sqlQueryInsert="INSERT INTO place(name, description, coordinate) VALUES ('"+placeDTO.getName()+"',+'"+placeDTO.getDescription()+"','"+placeDTO.getCoordinate()+"')";
        SQLiteDatabase database = this.getCon(context);
        try{
            database.execSQL(sqlQueryInsert);
            response = 1;
        }catch (Exception err){
            Log.w("model","insertPlace: catch => "+ err);
        }
        return response;
    }
}
